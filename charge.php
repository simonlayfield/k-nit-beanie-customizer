<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="container message">

<?php

  require_once('./stripeConfig.php');

  $token = $_POST['stripeToken'];
  $email = $_POST['stripeEmail'];
  $stripeShippingName = htmlspecialchars($_POST['stripeShippingName']);
  $stripeShippingAddressLine1 = htmlspecialchars($_POST['stripeShippingAddressLine1']);
  $stripeShippingAddressZip = htmlspecialchars($_POST['stripeShippingAddressZip']);
  $stripeShippingAddressCity = htmlspecialchars($_POST['stripeShippingAddressCity']);
  $stripeShippingAddressState = htmlspecialchars($_POST['stripeShippingAddressState']);
  $stripeShippingAddressCountry = htmlspecialchars($_POST['stripeShippingAddressCountry']);
  $stripeShippingAddressCountryCode = htmlspecialchars($_POST['stripeShippingAddressCountryCode']);
  $amount = $_POST['amount'];
  $style = Trim(stripslashes($_POST['style']));
  $pattern = Trim(stripslashes($_POST['pattern']));
  $selectedShipping = htmlspecialchars($_POST['selectedShipping']);
  $colours = Trim(stripslashes($_POST['colours']));
  $image = Trim(stripslashes($_POST['image']));

  $customer = \Stripe\Customer::create(array(
      'email' => $email,
      'card'  => $token
  ));

  try {

    $charge = \Stripe\Charge::create(array(
      "customer" => $customer->id,
      "amount" => $amount,
      "currency" => "gbp",
      "description" => "Custom Beanie"
      ));

  } catch(\Stripe\Error\Card $e) { ?>

    <h1 class="tac">Oops, there seems to have been a problem</h1>

    <p>We are afraid that your payment has been unsuccessful and we will be unable to complete your order at this time. Please feel free to try again and if you continue to have problems please <a href="mailto:hello@k-nit.co.uk">contact us</a> and we'll do everything we can to fix the problem.</p>

    <p class="tac">
        <a href="./" class="button">Try again</a>
    </p>

  <?php return; }

    define('UPLOAD_DIR', 'orders/');
    $image = str_replace('data:image/png;base64,', '', $image);
    $image = str_replace(' ', '+', $image);
    $data = base64_decode($image);
    $file = str_replace(array('@', '.'),"_",$email) . '_' . uniqid() . '.png';
    $fileUpload = UPLOAD_DIR . $file;
    $success = file_put_contents($fileUpload, $data);
    // print $success ? $file : 'Unable to save the file.';

    function return_output($template){
        ob_start(); ?>
        <?php
         global $style, $pattern, $selectedShipping, $colours, $amount, $file, $stripeShippingName, $stripeShippingAddressLine1, $stripeShippingAddressZip, $stripeShippingAddressCity, $stripeShippingAddressState, $stripeShippingAddressCountryCode;
         include($template); ?>
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    use \Mailjet\Resources;

    function sendEmail($recipient){

        global $email;

        require 'vendor/autoload.php';

        $apiKey = '674a804bdeeef3a151926605158c3b70';
        $secretKey = 'ac6d1a980f485a124d188e258caef31f';

        if ($recipient == 'customer') {
            $emailRecipient = $email;
            $emailFile = 'emails/confirmation-email.php';
        } else {
            $emailRecipient = 'hello@k-nit.co.uk';
            $emailFile = 'emails/confirmation-email-admin.php';
        }

        $mj = new \Mailjet\Client($apiKey, $secretKey);

        $body = [
          'FromEmail' => "hello@k-nit.co.uk",
          'FromName' => "K-nit",
          'Subject' => "Thanks for your custom beanie order",
          'Text-part' => "",
          'Html-part' => return_output($emailFile),
          'Recipients' => [['Email' => $emailRecipient]]
        ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

    }

    sendEmail('customer');
    sendEmail('admin');

    ?>

    <h1 class="tac">Thanks for your order</h1>

    <p>Thanks for customizing your very own K-nit beanie. We are happy to say that your order had been received successfully and we are really excited to get started.</p>

    <p>A confirmation email has been sent to the email that you provided with the details of your purchase.</p>

    <p class="tac">
        <a href="./" class="button">Design another beanie</a>
        <a href="http://k-nit.co.uk" class="button">Go to the K-nit website</a>
    </p>

</div>
</body>

</html>
