var helpers = Ractive.defaults.data;
helpers.decimalPrice = function(price){
    return (Math.floor(parseInt(price))/ 100).toFixed(2);
}

var ractive = new Ractive({
  el: '#customizer',
  template: '#template',
  data: data,
  bySix: function (index) {
      if (index % 6 == 0) {
          return true;
      } else {
          return false;
      }
  },
  setColours: function () {

    this.set('selectedColours', this.get('beanies[0].colors'));
    this.set('availablePatterns', this.get('beanies.' + this.get('activeStyleIndex') + '.patterns'));
    this.set('selectedColours', this.get('beanies.' + this.get('activeStyleIndex') + '.colors'));

    // Get the default beanie title
    var defaultBeaniePattern = this.get('availablePatterns.0').toLowerCase();

    var activeBeaniePattern = document.getElementsByClassName('beaniePreview__pattern--' + defaultBeaniePattern)[0];
    var paths = activeBeaniePattern.getElementsByTagName('svg')[0].children;

    for (var i=0; i < paths.length; i++) {
        $('.st' + i).addClass(this.get('selectedColours[' + i + ']'));
    }

 },
  computed: {

    hexColours: function () {

        var hexMap = this.get('hexMap');

        return this.get('selectedColours').map(function (value) {
            return hexMap[value];

        });

    },
    totalPrice: function () {
        var activeStyleIndex = this.get('activeStyleIndex');
        return parseInt(this.get('beanies.' + activeStyleIndex + '.price')) + parseInt(this.get('selectedShippingOption'));
    }
  },
  resetPattern: function (currentColourLimit) {

    // Check the new length of colours
    var newColourLimit = this.get('patterns.' + this.get('activePatternTitle') + '.colourlimit');

    // Stash or add the difference in colours
    if (newColourLimit < currentColourLimit) {

        var reducedColours = this.get('selectedColours').slice(0,newColourLimit),
            stashedColours = this.get('selectedColours').slice(newColourLimit);

        this.set('selectedColours', reducedColours);
        this.set('colourStash', stashedColours.concat(this.get('colourStash')));
        this.set('selectedColourIndex', 0);

    } else if (newColourLimit > currentColourLimit) {

        var colourStash = this.get('colourStash'),
            // Work out the difference in colour limits
            diffNum = newColourLimit - currentColourLimit,
            newColours = colourStash.slice(0,diffNum);

        this.set('colourStash', colourStash.slice(diffNum));
        this.set('selectedColours', this.get('selectedColours').concat(newColours));
    }
}
});

ractive.setColours();

ractive.observe('selectedColours', function (obj) {
    var colours = document.getElementsByClassName('colourOptions__swatchItem');
    for (var i = 0; i < colours.length; i++) {
        colours[i].classList.remove('active');
    }
   obj.forEach(function(colour){
       document.getElementById('colour-option-' + colour).classList.add('active');
   });
});

ractive.on('changeBeanie', function (event) {

    var beanieColours = this.get(event.keypath + '.colors');
    this.set('selectedColours', beanieColours);

    $.each($('.pattern.active svg').children(), function (index) {

        $('.st' + index).addClass(this.get('selectedColours[' + index + ']'));

    });

});

ractive.on('toggleBobble', function () {
    var activeStyleIndex = this.get('activeStyleIndex'),
    currentPrice = this.get('beanies[' + activeStyleIndex + '].price');
    this.toggle('beanies[' + activeStyleIndex + '].bobble');

    if (this.get('activeStyleTitle') === "bowenturnup" || this.get('activeStyleTitle') === "bowenskull") {
        if (this.get('beanies[' + activeStyleIndex + '].bobble')) {
            this.set('beanies[' + activeStyleIndex + '].price', parseInt(currentPrice) + 200);
        } else {
            this.set('beanies[' + activeStyleIndex + '].price', parseInt(currentPrice) - 200);
        }
    }
});

ractive.on('changeColour', function (event) {

    var selectedIndex = this.get('selectedColourIndex'),
    selectedColour = this.get('selectedColours[' + selectedIndex + ']');
    document.getElementById('colour-option-' + selectedColour).classList.remove('active');

    this.set('selectedColours[' + selectedIndex + ']', event.context);

    var selectedColour = this.get('selectedColours[' + selectedIndex + ']');
    document.getElementById('colour-option-' + selectedColour).classList.add('active');
});

ractive.on('changeSelected', function (event, index) {

    this.set('selectedColourIndex', index);

});

ractive.on('swapColours', function () {
    var selectedColours = this.get('selectedColours');
    selectedColours.sort(function() {
      return .5 - Math.random();
    });
    this.set('selectedColours', selectedColours);
});

ractive.on('randomizeColours', function () {

    var selectedColours = this.get('selectedColours'),
        activeWool = this.get('activeWool'),
        activeWool = activeWool + 'Colours',
        activeWool = this.get(activeWool);

    for (i = 0; i < selectedColours.length; i++) {
        var randomArray = Math.floor(Math.random() * activeWool.length),
            randomColour = Math.floor(Math.random() * activeWool[randomArray].length);
        this.set('selectedColours[' + i + ']', activeWool[randomArray][randomColour]);
    }
});

ractive.on('toggleCheckout', function (e) {

    if (this.get('showCheckout')) {
        this.set('selectedShippingOption', 0);

        if (this.get('selectedShippingId')) {
            document.getElementById(this.get('selectedShippingId')).checked = false;
        }

        var selectedShippingOptions = document.getElementsByClassName('selectedShippingOption');
        [].forEach.call(selectedShippingOptions, function(el){
            el.classList.remove('selectedShippingOption');
        });
    }

    var node = document.getElementById('checkout__image'),
        svg  = document.getElementById('svg--' + this.get('activePatternTitle')),
        xml  = new XMLSerializer().serializeToString(svg),
        data = "data:image/svg+xml;base64," + btoa(xml),
        img = new Image();
    img.setAttribute('src', data);

    var checkoutPattern = document.getElementById("checkout__pattern");

    if (checkoutPattern) {
        checkoutPattern.parentNode.removeChild(checkoutPattern);
    }

    img.onload = function() {
        var canvas = document.createElement('canvas'),
        emailImageField = document.getElementById('emailImageField');
        canvas.width = 400;
        canvas.height = 400;
        var context = canvas.getContext('2d');
        context.drawImage(img, 0, 0);

        var pngImage = document.createElement('img');
        pngImage.src = canvas.toDataURL('image/png');
        pngImage.setAttribute('id', 'checkout__pattern');
        document.getElementById('checkout__image').appendChild(pngImage);
        emailImageField.value = canvas.toDataURL('image/png');
    }


   this.toggle('showCheckout');

});

ractive.on('swipeLeft', function (event) {

    var activeBeanie = this.get('activeBeanie'),
    totalPanels = this.get('merinoColours').length;

    if (this.get('activePanel') == totalPanels-1) {
        return;
    } else {
        this.set('activePanel', this.get('activePanel') + 1 );
    }
});

ractive.on('swipeRight', function () {
    var activeBeanie = this.get('activeBeanie');
    if (this.get('activePanel') == 0) {
        return;
    } else {
        this.set('activePanel', this.get('activePanel') - 1 );
    }
});

ractive.on('setActiveStyle', function (event) {

    var activeStyleIndex = this.get('activeStyleIndex');

    // Check the colour limit - we may need to stash colours
    var currentColourLimit = this.get('patterns.' + this.get('activePatternTitle') + '.colourlimit');

    if (activeStyleIndex === this.get('beanies').length - 1) {
        this.set('activeStyleIndex', 0);
    } else {
        this.set('activeStyleIndex', activeStyleIndex + 1);
    }

    activeStyleIndex = this.get('activeStyleIndex');

    // Change the active style title
    this.set('activeStyleTitle', this.get('beanies.' + activeStyleIndex + '.title').toLowerCase().replace(/ /g,''));

    // Change available patterns based on style
    this.set('availablePatterns', this.get('beanies.' + activeStyleIndex + '.patterns'));
    this.set('activePatternIndex', 0);

    activePatternIndex = this.get('activePatternIndex');

    // Change the active style title
    this.set('activePatternTitle', this.get('availablePatterns.' + activePatternIndex).toLowerCase().replace(/ /g,''));

    this.resetPattern(currentColourLimit);

});

ractive.on('setActivePattern', function () {

    var activePatternIndex = this.get('activePatternIndex'),
    activeStyleIndex = this.get('activeStyleIndex'),
    patternLength = this.get('beanies.' + this.get('activeStyleIndex') + '.patterns').length;

    // Check the colour limit - we may need to stash colours
    var currentColourLimit = this.get('patterns.' + this.get('activePatternTitle') + '.colourlimit');

    if (activePatternIndex == patternLength - 1) {
        this.set('activePatternIndex', 0);
    } else {
        this.set('activePatternIndex', this.get('activePatternIndex') + 1);
    }

    activePatternIndex = this.get('activePatternIndex');

    // Change the active style title
    this.set('activePatternTitle', this.get('availablePatterns.' + activePatternIndex).toLowerCase().replace(/ /g,''));

    this.resetPattern(currentColourLimit);

});

ractive.on('addShipping', function (e) {

    var selectedShippingOptions = document.getElementsByClassName('selectedShippingOption');

    [].forEach.call(selectedShippingOptions, function(el){
        el.classList.remove('selectedShippingOption');
    });

    document.getElementById(e.node.id).classList.add('selectedShippingOption');
    this.set('selectedShippingOption', e.node.value);
    this.set('selectedShippingText', e.node.labels[0].innerText);
    this.set('selectedShippingId', e.node.id);

});

var handler = StripeCheckout.configure({
  key: 'pk_live_8guj0x4sSxPQ38dWhfozpuFe',
  image: 'https://k-nit.co.uk/custom/img/stripe-icon.png',
  locale: 'auto',
  token: function(token, addresses) {

        $('section.checkout').removeClass('active');
        $('#spinner').show();

        $("#stripeToken").val(token.id);
        $("#stripeEmail").val(token.email);

        $("#stripeShippingName").val(addresses.shipping_name);
        $("#stripeShippingAddressLine1").val(addresses.shipping_address_line1);
        $("#stripeShippingAddressZip").val(addresses.shipping_address_zip);
        $("#stripeShippingAddressState").val(addresses.shipping_address_state);
        $("#stripeShippingAddressCity").val(addresses.shipping_address_city);
        $("#stripeShippingAddressCountry").val(addresses.shipping_address_country);
        $("#stripeShippingAddressCountryCode").val(addresses.shipping_address_country_code);

        $("#stripeForm").submit();
    }
});

ractive.on('triggerStripe', function (e) {
    var shippingValidation = this.find('#shippingValidation'),
        selectedShippingOption = document.getElementsByClassName('selectedShippingOption');

    if (selectedShippingOption.length) {
        shippingValidation.classList.remove('error');
        handler.open({
          name: 'K-nit.co.uk',
          description: 'Custom beanie',
          shippingAddress: true,
          billingAddress: true,
          amount: this.get('totalPrice'),
          currency: 'gbp'
        });
    } else {
        shippingValidation.classList.add('error');
    }
    e.original.preventDefault();

});

// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});
