var data = {
    "title": "Pick your style",
    "description": "create your own beanie",
    "showCheckout": false,
    "selectedShippingOption": 0,
    "selectedColourIndex": 0,
    "activeStyleIndex": 0,
    "activeStyleTitle": "cormack",
    "activePatternIndex": 0,
    "activePatternTitle": "striangles",
    "colourStash": [],
    "availablePatterns": [],
    "activeWool": "merino",
    "selectedColours": ["turquoise", "charcoal", "cream", "cerise", "sunshine"],
    "merinoColours": [["white", "cream", "beige", "sunshine"], ["gold", "tangerine", "coral", "scarlet"], ["wine", "bordeaux", "cerise", "hotpink"], ["palepink", "pink", "lilac", "violet"], ["purple", "royal", "sky", "airforce"], ["mallard", "teal", "turquoise", "emerald"], ["bottle", "apple", "cactus", "basil"], ["toffee", "chocolate", "birch", "silver"], ["grey", "charcoal", "navy", "black"]],
    "activePanel": 0,
    "beanies":
        [{
            "title": "Cormack",
            "mask": "images/beanie-howitt.png",
            "price": "3800",
            "colors": ["turquoise", "charcoal", "cream", "cerise", "sunshine"],
            "availableColours": "merinoColours",
            "patterns": ["Striangles","Schwing", "Jig Jag", "Glory Boy", "Plain", "Contrast Pattern", "Nava Say Nava", "Nordicai"],
            "bobble": true,
            "url": "http://shop.k-nit.co.uk/collections/cormack"
        },
        {
            "title": "Cormack Turnup",
            "img": "images/howitt.jpg",
            "mask": "images/beanie-howitt.png",
            "price": "4000",
            "colors": ["turquoise", "charcoal", "cream", "cerise", "sunshine"],
            "availableColours": "merinoColours",
            "patterns": ["Striangles", "Plain", "Contrast Pattern", "Contrast Blast", "Wagga"],
            "bobble": true
        },
        {
            "title": "Bowen Turnup",
            "img": "images/howitt.jpg",
            "mask": "images/beanie-howitt.png",
            "defaultPattern": "plain.svg",
            "price": "3000",
            "colors": ["turquoise", "charcoal"],
            "availableColours": "merinoColours",
            "patterns": ["Plain"],
            "bobble": false,
            "url": "http://shop.k-nit.co.uk/collections/bowen-beanies"
        },
        {
            "title": "Bowen Skull",
            "img": "images/howitt.jpg",
            "mask": "images/beanie-howitt.png",
            "defaultPattern": "plain.svg",
            "price": "3000",
            "colors": ["turquoise", "charcoal"],
            "availableColours": "merinoColours",
            "patterns": ["Plain"],
            "bobble": false
        }],
    "patterns": {
        "striangles": {
            "colourlimit": 5
        },
        "jigjag": {
            "colourlimit": 5
        },
        "gloryboy": {
            "colourlimit": 5
        },
        "electricwool": {
            "colourlimit": 5
        },
        "burster": {
            "colourlimit": 5
        },
        "plain": {
            "colourlimit": 1
        },
        "contrastpattern": {
            "colourlimit": 2
        },
        "contrastblast": {
            "colourlimit": 4
        },
        "wagga": {
            "colourlimit": 5
        },
        "contrastblock": {
            "colourlimit": 2
        },
        "nordicai": {
            "colourlimit": 5
        },
        "navasaynava": {
            "colourlimit": 5
        },
        "schwing": {
            "colourlimit": 5
        }
    },
    "hexMap": {
        "white": "#fff",
        "cream": "#ffffdc",
        "beige": "#efc99a",
        "sunshine": "#e59c42",
        "gold": "#df9b1c",
        "tangerine": "#ff7d3e",
        "coral": "#ab3f40",
        "scarlet": "#c5000f",
        "wine": "#9b072b",
        "bordeaux": "#531f2b",
        "cerise": "#9b3d63",
        "hotpink": "#b7205c",
        "palepink": "#ffd0e3",
        "pink": "#d8768c",
        "lilac": "#b2728b",
        "violet": "#7c507d",
        "purple": "#47307c",
        "royal": "#3a4d96",
        "sky": "#96b4c6",
        "airforce": "#474e61",
        "mallard": "#3b494c",
        "teal": "#015e56",
        "turquoise": "#01a59a",
        "emerald": "#018848",
        "bottle": "#1e2a20",
        "apple": "#8eb247",
        "cactus": "#5c553c",
        "basil": "#77622d",
        "toffee": "#78422e",
        "choc": "#2e2818",
        "birch": "#a19c83",
        "silver": "#b9b1a3",
        "grey": "#6e6e6e",
        "charcoal": "#4f4f4f",
        "navy": "#15293d",
        "black": "#111111"
    },
    "shippingOptions": [
        {
            "option": "UK Standard 1st Shipping (£2.99)",
            "value": 299
        },
        {
            "option": "UK Signed For 1st Shipping (£3.99)",
            "value": 399
        },
        {
            "option": "UK Next Day Before 1pm (£6.99)",
            "value": 699
        },
        {
            "option": "UK Next Day Before 1pm Sat Guaranteed (£9.99)",
            "value": 999
        },
        {
            "option": "UK Standard 2nd Shipping (FREE)",
            "value": 0
        },
        {
            "option": "Int Standard Europe (FREE)",
            "value": 0
        },
        {
            "option": "Int Tracked and Signed Europe (£7.99)",
            "value": 799
        },
        {
            "option": "Int Standard Rest of the World (FREE)",
            "value": 0
        },
        {
            "option": "Int Tracked and Signed Rest of the World (£8.99)",
            "value": 899
        }
    ]

}
